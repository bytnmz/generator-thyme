'use strict';

export default class <%= _.pascalCase(name) %> {
  constructor($selector, selfInit = true) {
    this.name = '<%= name %>';
    console.log('%s module', this.name.toLowerCase());

    if(selfInit) this.init($selector);
  }

  init($selector) {

  }
}
