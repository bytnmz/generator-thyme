'use strict';

const Generator = require('yeoman-generator');

module.exports = class extends Generator {
  initializing() {
    let pages = this.config.get('pages');

    if(pages === undefined){
      pages = [];
      pages.push(this.arguments[0]);
    }
    else {
      pages.push(this.arguments[0]);
    }

    this.config.set('pages', pages);

    this.name = 'no-name';
    this.urlPath = '';
    if (this.arguments[0]) {
      this.name = this.arguments[0];
      this.urlPath = this.name.split('/').reduce((acc, ele, i) => {
        return acc + '../';
      }, '');
    }

    this.layout = 'base';
    if (this.options.layout) {
      this.layout = this.options.layout;
    }

    this.layoutDir = '_layouts';
  }

  writing() {
    const templateData = {
      name: this.name,
      urlPath: this.urlPath,
      layout: this.layout,
      layoutDir: this.layoutDir
    };

    this.fs.copyTpl(
      this.templatePath('page.pug'),
      this.destinationPath(`src/${this.arguments}/index.pug`),
      templateData
    );
  }
};