'use strict';

export default class SiteFooter {
  constructor($selector, breakpoints, selfInit = true) {
    if (selfInit) this.init($selector, breakpoints);
  }

  init($selector, breakpoints) {
    const $scrollTopBtn = $('.btn-scroll-top', $selector);

    $scrollTopBtn.on('click', e => {
      e.preventDefault();

      $('html, body').animate({
        scrollTop: 0
      });
    });
  }
}
