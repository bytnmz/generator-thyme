'use strict';

import AjaxListing from '../../common/ajax-listing/ajax-listing';
import SearchFilters from '../../common/search-filters/search-filters';

export default class Listing extends AjaxListing {
  constructor($selector, templateID) {
    super($selector, templateID);

    this.$filters = $('.search__filters', $selector);

    // Init search filters
    $('.search__filters', $selector).map((i, ele) => {
      new SearchFilters($(ele));
    });

    const filterPos = this.$filters.offset().top;
    const toggleBtnHeight = $('.search-toggle', $selector).outerHeight();

    $(window).on('scroll.listingScroll', e => {
      let screenTop = $(window).scrollTop();
      let screenBottom = screenTop + $(window).outerHeight();
      if (screenBottom > filterPos + toggleBtnHeight + 20) {
        $selector.addClass('in-view');
      } else {
        $selector.removeClass('in-view');
      }
    }).trigger('scroll.listingScroll');
  }
}
