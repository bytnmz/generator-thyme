'use strict';

import $ from 'jquery';

export default class NavDropdown {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;
    this.$list = $('ul', this.$selector);
    this.listShown = false;

    this.$triggerButton = $('<button class="nav-dropdown__button" type="button"><span class="label"></span><span class="icon icon-angle-down"></span></button>');
    this.$triggerLabel = this.$triggerButton.find('.label');

    // Self init to be true by default unless explicitly set to false through instance argument
    if (selfInit) this.init();
  }

  init() {
    if (this.$selector.find('.nav-dropdown__button').length) {
      this.$triggerButton = $('.nav-dropdown__button', this.$selector);
    } else {

      if (this.$list.find('.active').length) {
        this.$triggerLabel.text(this.$list.find('.active a').text());
      } else {
        this.$triggerLabel.text(this.$list.find('li:eq(0) a').text());
      }

      this.$selector.prepend(this.$triggerButton);
    }

    this.bindHandlers();
  }

  bindHandlers() {
    this.$triggerButton.on('click', (e) => {
      e.preventDefault();
      e.stopPropagation();

      if (this.listShown) {
        this.hideList();
      } else {
        window.emitter.emit('navDropdownOpened', this.$selector);
        this.showList();
      }
    });

    window.emitter.on('navDropdownOpened', ($openedSelector) => {
      if (this.$selector != $openedSelector) {
        this.hideList();
      }
    });

    $(document).on('click.navDropdown', (e) => {
      let $eTarget = $(e.target);

      if (!$eTarget.hasClass('nav-dropdown') && !$eTarget.parents('.nav-dropdown').length) {
        this.hideList();
      }
    });
  }

  showList() {
    this.$selector.addClass('list-shown');
    this.listShown = true;
  }

  hideList() {
    this.$selector.removeClass('list-shown');
    this.listShown = false;
  }
}
