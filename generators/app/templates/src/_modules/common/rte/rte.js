'use strict';

import TableResponsive from '../table-responsive/table-responsive';

export default class Rte {
  constructor($selector = $('.rte'), selfInit = true) {
    if (selfInit) this.init($selector);
  }

  init($selector) {
    if($('table', $selector).length){
			$('table', $selector).map((i, ele) => {
        new TableResponsive($(ele));
      });
    }
  }
}
