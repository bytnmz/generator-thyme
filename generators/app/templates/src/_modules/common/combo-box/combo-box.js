'use strict';

import $ from 'jquery';

export default class ComboBox {
  constructor($selector) {
    const $comboBoxTrigger = $('.combo-box__trigger', $selector);
    const $comboBoxTriggerText = $('.label', $comboBoxTrigger);
    const $comboBoxOptions = $('.combo-box__options', $selector);
    const $options = $('ul li', $comboBoxOptions);

    /* Setup Combobox Search */
    const $search = $('<div class="combo-box__search"><input type="text" placeholder="Enter keyword" /></div>');
    $comboBoxOptions.prepend($search);

    $comboBoxTrigger.on('click', e => {
      e.preventDefault();

      if ($selector.hasClass('opened')) {
        $selector.removeClass('opened');
        $search.find('input').val('').trigger('keyup');
      } else {
        $selector.addClass('opened');
      }
    });

    $options.map((i, opt) => {
      let $this = $(opt);
      let $value = $this.attr('value');

      $this.on('change', e => {
        let selectedLabels = [];
        let selectedOptions = [];

        if ($this.is(':checked')) {
          if ($value == 'all') {
            $('.option input[type="checkbox"]', $comboBoxOptions).prop('checked', false);
            $comboBoxTriggerText.text('All');
          } else {
            $('input[type="checkbox"][value="all"]', $comboBoxOptions).prop('checked', false);
            $('.option', $comboBoxOptions).map((i, ele) => {
              let $this = $(ele);
              let optionLabel = $('label', $this).text();
              let optionValue = $('input', $this).val();

              if ($('input', $this).is(':checked')) {
                selectedLabels.push(optionLabel);
                selectedOptions.push(optionValue);
              }
            });

            $comboBoxTriggerText.text(`${selectedLabels.join(', ')}`);
          }
        } else {
          if ($('.option input[type="checkbox"]:not([value="all"]):checked', $comboBoxOptions).length == 0) {
            /* Prevent "All" being unselected without any other options being selected */
            $('input[type="checkbox"][value="all"]', $comboBoxOptions).prop('checked', true);
            $comboBoxTriggerText.text('All');
          } else {
            $('input[type="checkbox"][value="all"]', $comboBoxOptions).prop('checked', false);
            $('.option', $comboBoxOptions).map((i, ele) => {
              let $this = $(ele);
              let optionLabel = $('label', $this).text();
              let optionValue = $('input', $this).val();

              if ($('input', $this).is(':checked')) {
                selectedLabels.push(optionLabel);
                selectedOptions.push(optionValue);
              }
            });

            $comboBoxTriggerText.text(`${selectedLabels.join(', ')}`);
          }
        }
      });
    });

    let searchTimeout;
    $search.find('input').on('keyup', e => {
      // this.search($(e.target).val());
      let term = $(e.target).val().toLowerCase();
      clearTimeout(searchTimeout);

      if (term.trim().length > 0) {
        searchTimeout = setTimeout(() => {
          $options.hide();
          $options.map((i, ele) => {
            let $this = $(ele);
            if ($this.find('label').text().toLowerCase().indexOf(term) > -1) {
              $this.show();
            }
          });
        }, 100);
      } else {
        $options.show();
      }
    });

    $(document).on('click.comboBox', e => {
      const $eTarget = $(e.target);

      if (!$eTarget.hasClass('combo-box') && !$eTarget.parents('.combo-box').length) {
        $selector.removeClass('opened');
        $search.find('input').val('').trigger('keyup');
      }
    });
  }
}
