'use strict';

import 'slick-carousel';

const _$items = new WeakMap();

export default class Accordion {
  constructor($selector, selfInit = true) {
    _$items.set(this, $selector.find('.accordion-item'));

    // Self init to be true by default unless explicitly set to false through instance argument
    if (selfInit) this.init($selector);
  }

  init($selector, config = {}) {
    const cfg = $.extend({
      autoCollapseSiblings: false,
      scrollOffset: 0,
      scrollToPosition: false
    }, config);

    const { scrollOffset, autoCollapseSiblings, scrollToPosition } = cfg;

    _$items.get(this).map((i, ele) => {
      const $item = $(ele);

      // Init accordion: wrap contents to form accordion
      const id = `#${$item.attr('id')}`;
      const title = $item.data('placeholder-label');

      const _accordionTrigger = `<a class="accordion-title" href="${id}"><h3>${title}</h3><span class="icon icon-angle-down"></span></a>`;
      $item.find('> *').wrapAll('<div class="accordion-content"></div>');               // do this. This will keep the existing bindings on elements that will be wrapped
      // const _accordionContent = `<div class="accordion-content">${$item.html()}</div>`;    // do not use this. It will remove existing bindings on elements that will be wrapped

      // replace accordion contents
      $item.prepend(`${_accordionTrigger}`);

      // binding accordion operations
      const $title = $item.find('.accordion-title');

      $title.on('click', (e) => {
        e.preventDefault();

        if ($item.hasClass('expanded')) {
          // window.history.pushState({}, '', `${window.location.pathname}`);
          this._collapseItem($item);
        } else {
          window.history.pushState({ id }, '', `${id}`);
          if (autoCollapseSiblings) {
            /* Auto hide expanded item */
            const $activeItem = $selector.find('.accordion-item.expanded');

            if ($activeItem.length > 0) {
              this._collapseItem($activeItem);
            }
          }

          this._expandItem($item, () => {
            if (scrollToPosition) {
              $('html, body').animate({
                scrollTop: $title.offset().top - scrollOffset,
              });
            }
          });
        }
      });
    });

    /* automatically expand first item only after window is fully loaded */
    $(window).on('load', () => {
      if (window.location.hash && $(window.location.hash).hasClass('accordion-item')) {
        return;
      }
      if (window.location.hash && $(window.location.hash).parents('.accordion-item')) {
        return;
      }
      // if ($selector.parents('.tab-content').length) {
      //   return;
      // }
      this._expandItem($selector.find('.accordion-item').first());
    });
  }

  _expandItem($item, callback = () => {}) {
    // window.history.pushState({ id: $item.attr('id') }, '', `${$item.attr('id')}`);
    $item.addClass('expanding');
    $('.accordion-content', $item).slideDown(() => {
      $item.addClass('expanded');
      $item.removeClass('expanding');
      callback();

      if($item.find('.card-carousel').length) {
        $('.card-carousel__body', $item).slick('setPosition');
      }
    });
  }

  _collapseItem($item, callback = () => {}) {
    // window.history.pushState({}, '', `${window.location.pathname}`);
    $item.removeClass('expanded');
    $('.accordion-content', $item).stop().slideUp(() => {
      callback();
    });
  }

  expandAll() {
    _$items.get(this).map((i, item) => {
      this._expandItem($(item));
    });
  }

  collapseAll() {
    _$items.get(this).map((i, item) => {
      this._collapseItem($(item));
    });
  }
}
