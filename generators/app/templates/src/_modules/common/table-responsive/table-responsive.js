'use strict';

export default class TableResponsive {
  constructor($selector, selfInit = true) {
    if (selfInit) this.init($selector);
  }

  init($selector) {
    $(window).on('resize.tableResponsive', () => {
      if (!$selector.parents('.table-responsive').length) {
        if ($selector.width() > $selector.parent().width()) {
          $selector.wrap('<div class="table-responsive" />');
        }
      }
    }).trigger('resize.tableResponsive');
  }
}
