'use strict';

import dot from 'dot';

export default class ListingBase {
  constructor() {
    this.template = `
    {{?it.length}}
      {{~it :item:index}}
        <li>
          <a href="{{=item.url}}">{{=item.name}}</a>
        </li>
      {{~}}
    {{??}}
      <li>No data.</li>
    {{?}}
    `;

    this.endpoint = '/apis/endpoint';
    this.urlHasQueryString = false;
    this.pageVariable = 'page';

    this.parameters = {
      [this.pageVariable]: 1
    };

    this.queries = this.getUrlQueries();

    this.beforeGetData = () => {
      console.log('Before calling AJAX');
    };

    this.getDataCallback = res => {
      console.log('Response from AJAX', res);
    };
  }

  getData(before = this.beforeGetData, cb = this.getDataCallback) {
    before();

    delete this.parameters.undefined;

    $.ajax({
      url: this.endpoint,
      method: 'GET',
      data: this.parameters,
      success: (res) => {
        cb(res);
      },
      error: (err) => {
        console.log(err);
      }
    });
  }

  renderTemplate(data, $container, toAppend = false) {
    /*
      data = [
        ..., { item object }
      ]
     */
    let dotTemplate = dot.template(this.template)(data);

    if (toAppend) {
      $container.append(dotTemplate);
    } else {
      $container.html(dotTemplate);
    }
  }

  updateTotalResults(total, $listing) {
    $('.total', $listing).text(total);
  }

  getUrlQueries() {
    let queryString = {};
    let query = window.location.search.substring(1);
    if (!query.length) {
      return queryString;
    }
    let vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
      let pair = vars[i].split("=");

      /* If first entry with this name */
      if (typeof queryString[pair[0]] === "undefined") {
        queryString[pair[0]] = decodeURIComponent(pair[1]);

        /* If second entry with this name */
      } else if (typeof queryString[pair[0]] === "string") {
        let arr = [ queryString[pair[0]], decodeURIComponent(pair[1])];
        queryString[pair[0]] = arr;

        /* If third or later entry with this name */
      } else {
        queryString[pair[0]].push(decodeURIComponent(pair[1]));
      }
    }
    return queryString;
  }

  updateURL(replaceState = false) {
    let queryString = '?';

    let i = 0;
    delete this.parameters.undefined;
    for (let param in this.parameters) {
      if (i > 0) {
        queryString += '&';
      }

      queryString += param + '=' + encodeURIComponent(this.parameters[param]);
      i++;
    }

    for (let query in this.queries) {
      if (typeof(this.parameters[query]) == 'undefined') {
        if (i > 0) {
          queryString += '&';
        }

        queryString += query + '=' + encodeURIComponent(this.queries[query]);
        i++;
      }
    }

    /**
    - reset this.queries to empty object after it is used for first call
    - to prevent it's parameters from being added into subsequent calls
    */
    if (this.urlHasQueryString) {
      this.queries = {};
      this.urlHasQueryString = false;
    }

    if (replaceState) {
      window.history.replaceState(this.parameters, '', queryString);
    } else {
      window.history.pushState(this.parameters, '', queryString);
    }
  }

  setupCheckboxes(fieldName) {
    /*
      - used by listing pages which has checkboxes of name="{fieldName}" with "All" options
      - to be called upon page load
      - will set {this.parameters[fieldName]} with the value of {this.queries[fieldName]}
    */

    this.parameters[fieldName] = (this.queries[fieldName]) ? this.queries[fieldName] : 'all';

    this.updateCheckboxView(fieldName);

    this._addCheckboxListener(fieldName);
  }

  updateCheckboxView(fieldName) {
    /*
    - to be called whenever the {fieldName} checkboxes needs to react based on {this.parameters[fieldName]}
    - will set the 'checked' prop for the selected {fieldName} checkboxes based on the {this.parameters[fieldName]}
     */

    if (this.parameters[fieldName] == 'all') {
      $(`input[name="${fieldName}"]`).map((i, ele) => {
        let $this = $(ele);

        if ($this.val() == 'all') {
          $this.prop('checked', true).trigger('change');
        } else {
          $this.prop('checked', false).trigger('change');
        }
      });
    } else {
      let values = this.parameters[fieldName].split('|');
      $(`input[name="${fieldName}"][value="all"]`).prop('checked', false);
      $(`input[name="${fieldName}"]`).map((i, ele) => {
        let $this = $(ele);

        if (values.indexOf($this.val()) !== -1) {
          $this.prop('checked', true).trigger('change');
        } else {
          $this.prop('checked', false).trigger('change');
        }
      });
    }
  }

  getCheckboxValues(fieldName) {
    /*
      - will return the piped-value string based on selected checkboxes under input[name="{fieldName}"]
    */
    let values = [];
    $(`input[name="${fieldName}"]`).toArray().map((ele) => {
      let $this = $(ele);

      if ($this.is(':checked')) {
        values.push($this.val());
      }
    });

    return values.join('|');
  }

  _addCheckboxListener(fieldName) {
    /*
      - for checkbox filters that do not trigger ajax call immediately
      - to be called to add event listener to the checkbox of {fieldName}
      - on every change of value, this will:
      - update this.parameters[fieldName]
      - uncheck all the other checkboxes when "ALL" is being checked
      - uncheck "ALL" when any of other checkbox is being checked
      - check "ALL" when all other checkboxes are being unchecked
    */

    $(`input[name="${fieldName}"]`).map((i, ele) => {
      let $this = $(ele);
      $this.on("change", (e) => {
        if ($this.prop("checked") == true) {
          if ($this.val() == "all") {
            /* uncheck all other checkbox when ALL is checked, and hide all subgroups */
            $(`input[name="${fieldName}"]`).prop("checked", false);
            $this.prop("checked", true);
          } else {
            $(`input[name="${fieldName}"][value="all"]`).prop("checked", false);
          }
        } else {
          if ($this.val() == "all") {
            /* ALL checkbox cannot be unchecked */
            $this.prop("checked", true);
          } else {
            if ($(`input[name="${fieldName}"]:checked`).length == 0) {
              /* when no checkbox is checked, ALL will be checked automatically */
              $(`input[name="${fieldName}"][value="all"]`).prop("checked", true);
            }
          }
        }
      });
    });

    /*
      - for checkboxes filters with "ALL" option and if each change in value triggers an ajax call immediately
      - to be called to add event listener to the checkbox of {fieldName}
      - on every change of value (delay of 300ms), this will:
      - update this.parameters[fieldName]
      - uncheck all the other checkboxes when "ALL" is being checked
      - uncheck "ALL" when any of other checkbox is being checked
      - check "ALL" when all other checkboxes are being unchecked
      - always reset this.parameters.page = 1
      - call {this.getData()}
      - call {this.updateURL()}
    */
    // let checkingTimeout;
    // $(`input[name="${fieldName}"]`).map((i, ele) => {
    //   let $this = $(ele);
    //   $this.on('change', e => {
    //     if ($this.prop('checked') == true) {
    //       if ($this.val() == 'all') {
    //         /* uncheck all other checkbox when ALL is checked, and hide all subgroups */
    //         $(`input[name="${fieldName}"]`).prop('checked', false);
    //         $this.prop('checked', true);
    //         this.parameters[fieldName] = this.getCheckboxValues(fieldName);
    //         this.parameters[this.pageVariable] = 1;
    //         this.getData();
    //         this.updateURL();
    //       } else {
    //         $(`input[name="${fieldName}"][value="all"]`).prop('checked', false);
    //         clearTimeout(checkingTimeout);
    //         checkingTimeout = setTimeout(() => {
    //           this.parameters[fieldName] = this.getCheckboxValues(fieldName);
    //           this.parameters[this.pageVariable] = 1;
    //           this.getData();
    //           this.updateURL();
    //         }, 300);
    //       }
    //     } else {
    //       if ($this.val() == 'all') {
    //         /* ALL checkbox cannot be unchecked */
    //         $this.prop('checked', true);
    //       } else {
    //         if ($(`input[name="${fieldName}"]:checked`).length == 0) {
    //           /* when no checkbox is checked, ALL will be checked automatically */
    //           $(`input[name="${fieldName}"][value="all"]`).prop('checked', true);
    //         }
    //         clearTimeout(checkingTimeout);
    //         checkingTimeout = setTimeout(() => {
    //           this.parameters[fieldName] = this.getCheckboxValues(fieldName);
    //           this.parameters[this.pageVariable] = 1;
    //           this.getData();
    //           this.updateURL();
    //         }, 300);
    //       }
    //     }
    //   });
    // });
  }

  setupSearchbar(fieldName) {
    this.parameters[fieldName] = (this.queries[fieldName]) ? this.queries[fieldName] : '';
    this.updateSearchbarView(fieldName);

    /**
    - Toggle if API is triggered every keyboard input
     */
    // this._addSearchbarListener(fieldName);
  }

  updateSearchbarView(fieldName) {
    const query = this.parameters[fieldName].replace(/\+/g, ' ');
    $(`input[name="${fieldName}"]`).val(query);
  }

  _addSearchbarListener(fieldName) {
    let inputTimeout;
    $(`input[name="${fieldName}"]`).on('input', e => {
      let $this = $(e.target);
      clearTimeout(inputTimeout);

      inputTimeout = setTimeout(() => {
        this.parameters[fieldName] = $this.val();
        this.parameters[this.pageVariable] = 1;
        this.getData();
        this.updateURL();
      }, 400);
    });
  }

  setupRadioButton(fieldName) {
    this.parameters[fieldName] = (this.queries[fieldName]) ? this.queries[fieldName] : $(`input[name="${fieldName}"][type='radio']:eq(0)`).val();

    this.updateRadioButtonView(fieldName);

    /**
    - Toggle if API is triggered on radio change
     */
    // this._addRadioButtonListener(fieldName);
  }

  updateRadioButtonView(fieldName) {
    $(`input[name="${fieldName}"]`).map((i, ele) => {
      let $this = $(ele);
      if ($this.val() == this.parameters[fieldName]) {
        $this.prop('checked', true);
      } else {
        $this.prop('checked', false);
      }
    });
  }

  getRadioButtonValue(fieldName) {
    let value;
    $(`input[name="${fieldName}"]`).map((i, ele) => {
      let $this = $(ele);
      if ($this.prop('checked') == true) {
        value = $this.val();
        return false; // break the loop once value is found
      }
    });

    return value;
  }

  _addRadioButtonListener(fieldName) {
    /**
    - for radio filters that triggers an ajax call immediately
     */
    $(`input[name="${fieldName}"]`).map((i, ele) => {
      let $this = $(ele);
      $this.on('change', e => {
        if ($this.prop('checked') == true)
          this.parameters[fieldName] = $this.val();

        this.getData();
        this.updateURL();
      });
    });
  }

  setupSelect(fieldName) {
    this.parameters[fieldName] = (this.queries[fieldName]) ? this.queries[fieldName] : $(`select[name='${fieldName}'] option:eq(0)`).val();

    this.updateSelectView(fieldName);

    /**
    - Toggle if API is triggered on select change
     */
    this._addSelectListener(fieldName);
  }

  updateSelectView(fieldName) {
    $(`select[name="${fieldName}"]`).val(this.parameters[fieldName]);

    const $triggerLabel = $(`select[name="${fieldName}"]`).next().find('.label');
    const $dropdown = $(`select[name="${fieldName}"]`).next().find('.custom-list');
    const label = $(`select[name="${fieldName}"]`).find(`option[value="${ this.parameters[fieldName] }"]`).text();

    // set active option in dropdown
    $dropdown.find('.active').removeClass('active');
    $dropdown.find(`[data-value="${this.parameters[fieldName]}"]`).addClass('active');

    // change label text on trigger button
    $triggerLabel.text(label);
  }

  _addSelectListener(fieldName) {
    // On change handlers for sorters
    const sorterParentClass = '.search__results-ctrl';
    $(`${sorterParentClass} select[name="${fieldName}"]`).on('change', () => {
      this.parameters[fieldName] = $(`select[name="${fieldName}"]`).val();
      this.parameters[this.pageVariable] = 1;
      this.getData();
      this.updateURL();
    });

    // On change handlers for non sorters
    // const selectFieldParentClass = '.search__filters';
    // $(`${selectFieldParentClass} select[name="${fieldName}"]`).on('change', () => {
    //   this.parameters[fieldName] = $(`select[name="${fieldName}"]`).val();
    // });
  }

  setupPagination(fieldName) {
    this.parameters[fieldName] = (this.queries[fieldName]) ? parseInt(this.queries[fieldName]) : 1;
  }

  reInitPagination(fieldName, pagination, $pagination, currentPage, totalPages) {
    let callback = () => {
      this._addPaginationListener(fieldName, $pagination);
    }

    this.updatePaginationView(pagination, currentPage, totalPages, callback);
  }

  _addPaginationListener(fieldName, $pagination) {
    $pagination.find('li').map((i, ele) => {
      const $this = $(ele);
      const $link = $this.find('a');

      $link.on('click', e => {
        e.preventDefault();

        if (!$this.hasClass('disabled')) {
          this.parameters[fieldName] = $link.data('page');

          this.getData();
          this.updateURL();
        }
      });
    });

    /* Mobile pagination */
    $pagination.next().find('.m-pagination-ctrl').map((i, ele) => {
      const $this = $(ele);
      const $link = $this.find('a');

      $link.on('click', e => {
        e.preventDefault();

        if (!$this.hasClass('disabled')) {
          this.parameters[fieldName] = $link.data('page');

          this.getData();
          this.updateURL();
        }

      });
    });
  }

  updatePaginationView(pagination, currentPage, totalPages, callback) {
    pagination.update(currentPage, totalPages, callback);
  }

  setupListeners(allFields){
    for(let i = 0; i < allFields.length; i++){
      if (allFields[i].type === 'text') this.setupSearchbar(allFields[i].name);
      if (allFields[i].type === 'select') this.setupSelect(allFields[i].name);
      if (allFields[i].type === 'checkbox') this.setupCheckboxes(allFields[i].name);
      if (allFields[i].type === 'radio') this.setupRadioButton(allFields[i].name);
    }
  }

  updateViews(allFields){
    for(let i = 0; i < allFields.length; i++){
      if (allFields[i].type === 'text') this.updateSearchbarView(allFields[i].name);
      if (allFields[i].type === 'select') this.updateSelectView(allFields[i].name);
      if (allFields[i].type === 'checkbox') this.updateCheckboxView(allFields[i].name);
      if (allFields[i].type === 'radio') this.updateRadioButtonView(allFields[i].name);
    }
  }

  setupReset($reset, referenceToFields, searchFilters = null, sorterParentClass = '.search__results-ctrl') {
    $reset.on('click', (e) => {
      e.preventDefault();

      this.handleReset(referenceToFields, sorterParentClass);

      if (searchFilters) {
        searchFilters.reset();
      }

      this.getData();
      this.updateURL();
      
      // close filter popup
      // this.closeFilterPopup($('.search__filters-wrapper.active', this.$filters));
    });
  }

  handleReset(referenceToFields, sorterParentClass) {
    for (let i = 0; i < referenceToFields.length; i++) {
      if (referenceToFields[i].type === "text") {
        this.parameters[referenceToFields[i].name] = "";
        this.updateSearchbarView(referenceToFields[i].name);
      }

      if (referenceToFields[i].type === "checkbox") {
        this.parameters[referenceToFields[i].name] = "all";
        this.updateCheckboxView(referenceToFields[i].name);
      }

      if (referenceToFields[i].type === "select") {
        // skip sorter reset
        if($(`select[name="${referenceToFields[i].name}"]`).parents(`${sorterParentClass}`).length){
          continue;
        }
        this.parameters[referenceToFields[i].name] = $(`select[name="${referenceToFields[i].name}"] option:eq(0)`).val();
        this.updateSelectView(referenceToFields[i].name);
      }

      if (referenceToFields[i].type === "radio") {
        this.parameters[referenceToFields[i].name] = $(`input[name="${referenceToFields[i].name}"][type='radio']:eq(0)`).val();
        this.updateRadioButtonView(referenceToFields[i].name);
      }
    }

    this.parameters[this.pageVariable] = 1;
  }

  _addSubmitButtonListener($button, fields, $instance = null) {
    $button.on('click', (e) => {
      e.preventDefault();
      this.parameters = this.getParameters(fields, true, $instance);

      this.getData();
      this.updateURL();

      // close filter popup
      // this.closeFilterPopup($('.search__filters-wrapper.active', this.$filters));
    });
  }

  makeInputObject($ele, type, array) {
    if(!$ele.length){
      return array;
    }
    else {
      $ele.map((i, ele) => {
        let inputObject = {};
        inputObject.name = $(ele).attr("name");
        inputObject.type = type;
        array.push(inputObject);
      });

      return array;
    }
  }

  makeOptionObject($ele, type, array) {
    if(!$ele.length){
      return array;
    }
    else {
      $ele.map((i, ele) => {
        let inputObject = {};
        // :eq(0) is used push only name of first radio button in each radio button group, does not apply to checkboxes
        inputObject.name = $(ele).find(`input[type='${type}']:eq(0)`).attr('name');
        inputObject.type = type;
        array.push(inputObject);
      });

      return array;
    }
  }

  getAllFields($parent){
    let allFields = [];

    if ($parent.find('.text-field input').length)
      allFields = this.makeInputObject($parent.find('.text-field input'), "text", allFields);
    if ($parent.find('select').length)
      allFields = this.makeInputObject($parent.find('select'), "select", allFields);
    if ($parent.find('.checkboxes').length)
      allFields = this.makeOptionObject($parent.find('.checkboxes'), "checkbox", allFields);
    if ($parent.find('.combo-box').length)
      allFields = this.makeOptionObject($parent.find('.combo-box'), "checkbox", allFields);
    if ($parent.find('.radio-buttons').length)
      allFields = this.makeOptionObject($parent.find('.radio-buttons'), "radio", allFields);

    return allFields;
  }

  getParameters(allFields, hasPagination = true, $instance = null) {
    let parameters = {};

    allFields.forEach(field => {
      if(field.type === 'checkbox')
        parameters[field.name] = this.getCheckboxValues(field.name);
      else if(field.type === 'radio')
        parameters[field.name] = this.getRadioButtonValue(field.name);
      else if(field.type === 'select')
        parameters[field.name] = $(`select[name=${field.name}]`, $instance).val();
      else
        parameters[field.name] = $(`input[name=${field.name}]`, $instance).val();
    });

    // add page key if listing has pagination
    if (hasPagination) parameters[this.pageVariable] = 1;

    delete parameters.undefined;

    return parameters;
  }

  closeFilterPopup($ele) {
    $ele.removeClass('active');
  }
}
