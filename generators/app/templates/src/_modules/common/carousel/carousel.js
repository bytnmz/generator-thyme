'use strict';

import 'slick-carousel';

export default class Carousel {
  constructor($selector, selfInit = true) {
    if(selfInit) this.init($selector);
  }

  init($selector) {
    const options = JSON.parse($selector.data('options'));

    $selector.on('init', (e, slick) => {
      this._onPrint($selector);
    });

    $selector.slick(options);

    $selector.on('beforeChange', (e, slick, currentSlide, nextSlide) => {
      // add before change script
    });

    $selector.on('afterChange', (e, slick, currentSlide) => {
      // add after  change script
    });
  }

  _onPrint($carousel){
		let beforePrint = function () {
			console.log('Functionality to run before printing.');
			$carousel.slick('setPosition');
		};

		let afterPrint = function () {
			console.log('Functionality to run after printing');
			$carousel.slick('setPosition');
		};

		if (window.matchMedia) {
			let mediaQueryList = window.matchMedia('print');

			mediaQueryList.addListener(function (mql) {
				if (mql.matches) {
					beforePrint();
				} else {
					afterPrint();
				}
			});
		}

		window.onbeforeprint = beforePrint;
		window.onafterprint = afterPrint;
	}
}
