'use strict';

import flatpickr from 'flatpickr';

export default class DateField {
  constructor($selector, selfInit = true) {
    // Self init to be true by default unless explicitly set to false through instance argument
    if (selfInit) this.init($selector);

    this.$selector = $selector;
  }

  init($selector) {
    let startDate, endDate;

    const $fromField = $('.date-field__inputs--from .date-picker', $selector);
    const $toField = $('.date-field__inputs--to .date-picker', $selector);

    this.datePickerFrom = $fromField.flatpickr({
      dateFormat: 'Y-m-d',
      altFormat: 'd M Y',
      altInput: true,
      disableMobile: true,
      onChange: (selectedDates, dateStr) => {         
        this.datePickerTo.set('minDate', dateStr);
      }
    });

    this.datePickerTo = $toField.flatpickr({
      dateFormat: 'Y-m-d',
      altFormat: 'd M Y',
      altInput: true,
      disableMobile: true,
      onChange: (selectedDates, dateStr) => {
        this.datePickerFrom.set('maxDate', dateStr);
      }
    });
  }

  reset() {
    this.datePickerFrom.clear();
    this.datePickerTo.clear();
  }
}
